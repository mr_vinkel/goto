package gt

import (
	"fmt"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"slices"
	"strings"

	giturls "github.com/whilp/git-urls"
	"gopkg.in/ini.v1"
)

func GetRepository(directory string) (map[string]interface{}, error) {
	root, err := getGitRoot(directory)
	if err != nil {
		return nil, err
	}

	remote, err := getRemote(root)
	if err != nil {
		return nil, err
	}

	remoteUrl, err := transformRemote(remote)
	if err != nil {
		return nil, err
	}

	branches, err := GetBranches(root)
	if err != nil {
		return nil, err
	}
	defaultBranch := "master"
	if slices.Contains(branches, "main") {
		defaultBranch = "main"
	}

	branch, err := getCurrentBranch(root)
	if err != nil {
		// a lot of shortcuts don't use current branch so just ignore it
		fmt.Println("failed to get current branch")
	}

	repository := map[string]interface{}{
		"remote": map[string]interface{}{
			"raw":        remote,
			"parsed":     remoteUrl.String(),
			"scheme":     remoteUrl.Scheme,
			"host":       remoteUrl.Host,
			"hostname":   remoteUrl.Hostname(),
			"path":       remoteUrl.Path,
			"repository": path.Base(remoteUrl.Path),
			"group":      path.Dir(remoteUrl.Path),
		},
		"branch": map[string]interface{}{
			"default": defaultBranch,
			"current": branch,
		},
	}
	return repository, nil
}

func GetBranches(directory string) ([]string, error) {
	result := []string{}
	root, err := getGitRoot(directory)
	if err != nil {
		return result, err
	}
	remotes := path.Join(root, ".git", "refs", "remotes", "origin")
	files, err := os.ReadDir(remotes)
	if err != nil {
		return result, err
	}
	for _, f := range files {
		result = append(result, f.Name())
	}
	return result, nil
}

func GetTags(directory string) ([]string, error) {
	result := []string{}
	root, err := getGitRoot(directory)
	if err != nil {
		return result, err
	}
	remotes := path.Join(root, ".git", "refs", "tags")
	files, err := os.ReadDir(remotes)
	if err != nil {
		return result, err
	}
	for _, f := range files {
		result = append(result, f.Name())
	}
	return result, nil
}

func getRemote(root string) (string, error) {
	configPath := path.Join(root, ".git", "config")
	cfg, err := ini.Load(configPath)
	if err != nil {
		return "", fmt.Errorf("error opening %s: %w", configPath, err)
	}
	remote := cfg.Section("remote \"origin\"")

	if remote == nil {
		return "", fmt.Errorf("no remote origin")
	}

	url, err := remote.GetKey("url")
	if err != nil {
		return "", fmt.Errorf("no url for remote origin")
	}

	return url.String(), nil
}

func getCurrentBranch(root string) (string, error) {
	headPath := path.Join(root, ".git", "HEAD")
	content, err := os.ReadFile(headPath)
	if err != nil {
		return "", fmt.Errorf("failed to open %s: %w", headPath, err)
	}

	rx := regexp.MustCompile("ref: refs/heads/(.*)")
	matches := rx.FindAllStringSubmatch(string(content), -1)
	if len(matches) == 1 && len(matches[0]) == 2 {
		return matches[0][1], nil
	}

	return string(content), fmt.Errorf("HEAD do not point to a branch")
}

func getGitRoot(directory string) (string, error) {
	dir, err := filepath.Abs(directory)
	if err != nil {
		return "", err
	}

	for {
		files, err := os.ReadDir(dir)
		if err != nil {
			return "", err
		}
		for _, f := range files {
			if f.Name() == ".git" {
				return dir, nil
			}
		}
		if dir == "/" {
			return "", fmt.Errorf("not in a git repository dummy")
		}
		dir = filepath.Dir(dir)
	}
}

func transformRemote(remote string) (*url.URL, error) {
	if remote == "" {
		return nil, fmt.Errorf("remote is empty")
	}

	remoteUrl, err := giturls.Parse(remote)
	if err != nil {
		return nil, fmt.Errorf("failed to parse remote '%s': %v", remote, err)
	}

	// remove .git suffix
	path, _ := strings.CutSuffix(remoteUrl.Path, ".git")

	if remoteUrl.Host == "" || path == "" {
		return nil, fmt.Errorf("failed to parse remote '%s'", remote)
	}

	httpUrl := url.URL{
		Scheme: "https",
		Host:   remoteUrl.Host,
		Path:   strings.TrimPrefix(path, "/"),
	}

	return &httpUrl, nil
}
