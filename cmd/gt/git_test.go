package gt

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTransformRemote(t *testing.T) {
	tests := []struct {
		remote, expected string
		err              bool
	}{
		// Garbage
		{"", "", true},
		{"potato", "", true},

		{"git@gitlab.com:mr_vinkel/gitlab-monitor.git", "https://gitlab.com/mr_vinkel/gitlab-monitor", false},
		{"https://gitlab.com/mr_vinkel/gitlab-monitor.git", "https://gitlab.com/mr_vinkel/gitlab-monitor", false},
		{"https://user:pw@gitlab.com/mr_vinkel/gitlab-monitor.git", "https://gitlab.com/mr_vinkel/gitlab-monitor", false},
	}

	for _, tt := range tests {
		result, err := transformRemote(tt.remote)
		if tt.err {
			assert.Error(t, err, tt)
		} else {
			assert.Equal(t, tt.expected, result.String())
		}
	}
}

func TestGit(t *testing.T) {
	repo, err := GetRepository(".")

	assert.Nil(t, err)
	assert.Contains(t, Get(repo, "remote.raw"), "mr_vinkel/goto")
	assert.Contains(t, Get(repo, "remote.raw"), "gitlab.com")
	assert.Equal(t, "https://gitlab.com/mr_vinkel/goto", Get(repo, "remote.parsed"))
	assert.Equal(t, "gitlab.com", Get(repo, "remote.host"))
	assert.Equal(t, "mr_vinkel/goto", Get(repo, "remote.path"))
	assert.Equal(t, "goto", Get(repo, "remote.repository"))
	assert.NotEmpty(t, Get(repo, "branch.current"))
	assert.Equal(t, "main", Get(repo, "branch.default"))
}

func TestGetBranches(t *testing.T) {
	branches, err := GetBranches(".")

	assert.Nil(t, err)
	assert.NotEmpty(t, branches)
	assert.Contains(t, branches, "main")
}

func TestGetTags(t *testing.T) {
	tags, err := GetTags(".")

	assert.Nil(t, err)
	assert.NotEmpty(t, tags)
	assert.Contains(t, tags, "0.0.10")
}
