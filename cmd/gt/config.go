package gt

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
)

const (
	EnvVarConfigFile      = "GT_CONFIG_FILE"
	defaultConfigFilePath = "${HOME}/.config/gt/config.yaml"
)

type Config struct {
	Version      string     `yaml:"version"`
	Editor       string     `yaml:"editor"`
	SearchDirs   []string   `yaml:"searchDirs"`
	Globals      []Shortcut `yaml:"globals"`
	Repositories []Shortcut `yaml:"repositories"`
}

type Shortcut struct {
	Name        string      `yaml:"name"`
	Description string      `yaml:"description"`
	Alias       string      `yaml:"alias"`
	Host        string      `yaml:"host"`
	Target      string      `yaml:"target"`
	Templating  bool        `yaml:"templating"`
	Parameters  []Parameter `yaml:"parameters"`
}

type Parameter struct {
	Name           string `yaml:"name"`
	Description    string `yaml:"description"`
	Alias          string `yaml:"alias"`
	Flag           bool   `yaml:"falg"`
	Target         string `yaml:"target"`
	Templating     bool   `yaml:"templating"`
	CompletionFunc string `yaml:"completionFunc"`
}

func ReadConfig() (*Config, error) {
	config := Config{}

	file := os.Getenv(EnvVarConfigFile)
	if file == "" {
		file = os.ExpandEnv(defaultConfigFilePath)
	}

	// create config file is it doesn't exist
	if _, err := os.Stat(file); errors.Is(err, os.ErrNotExist) {
		parent := filepath.Dir(file)
		err := os.MkdirAll(parent, 0700)
		if err != nil {
			return nil, err
		}
		err = os.WriteFile(file, []byte(templateStr), 0644)
		if err != nil {
			return nil, err
		}
	}

	content, err := os.ReadFile(file)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(content, &config)
	return &config, err
}

func ConfigCmd(config *Config) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "config",
		Short: "Open the GoTo config for editing",
		Args:  cobra.NoArgs,
		RunE:  func(cmd *cobra.Command, args []string) error { return openConfig(config) },
		ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			return []string{}, cobra.ShellCompDirectiveNoFileComp
		},
	}
	return cmd
}

func openConfig(config *Config) error {
	file := os.Getenv(EnvVarConfigFile)
	if file == "" {
		file = os.ExpandEnv(defaultConfigFilePath)
	}

	editor := config.Editor
	if editor == "" {
		editor = "nano"
	}

	cmd := exec.Command(editor, file)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Start()
	if err != nil {
		return err
	}
	return cmd.Wait()
}

const templateStr = `version: "1.0"
# Editor to open this file in with 'gt config' - default is nano
editor: nano
# Directories to search for git repositories. If the search path contains many files this will be slow - default is $HOME
searchDirs:
- $HOME
globals:
## Global shortcuts can be called from anywhere
#- name: google
#  alias: google
#  description: Open google
#  target: https://google.com/search?q=${arg.escaped}

## Repository shortcuts only work when in a git repository
repositories:
#- name: repository
#  alias: r
#  description: Open repository
## Host is used to only use this target for remotes with this host - allowing for using the same shortcut alias for github, gitlab etc.
#  host: my-server.com
#  target: https://my-server.com/${git.remote.repository}
#  parameters:
#  - name: branch
#    alias: b
#    description: Open branch
## Target can be overridden with parameters
#    target: https://my-server.com/${git.remote.repository}?ref=${param.branch}
## CompletionFunc can be used specify completion values. Default is none. Allowed functions are: none, files, branches, tags
#    completionFunc: branches
#  - name: current
#    alias: c
#    description: Current branch
## Flags can be used to specify a parameter without input
#    flag: true
## Use golang text templating instead of expand - see https://pkg.go.dev/text/template and https://masterminds.github.io/sprig
#    templating: true
#    target: https://my-server.com/{{.git.remote.repository}}?ref={{.git.branch.current|lower}}
`
