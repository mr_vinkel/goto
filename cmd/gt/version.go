package gt

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/spf13/cobra"
)

var Version = "dev"
var latestRelease = "https://gitlab.com/api/v4/projects/mr_vinkel%2Fgoto/releases/permalink/latest"

func VersionCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Show GoTo version",
		Args:  cobra.NoArgs,
		RunE:  printVersion,
		ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			return []string{}, cobra.ShellCompDirectiveNoFileComp
		},
	}
	cmd.Flags().BoolP("check", "c", false, "Check if current version is the latest")
	return cmd
}

func printVersion(cmd *cobra.Command, args []string) error {
	check, err := cmd.Flags().GetBool("check")
	if err != nil {
		return err
	}

	fmt.Printf("GoTo %s\n", Version)
	if check {
		return checkVersion(cmd)
	}
	return nil
}

func checkVersion(cmd *cobra.Command) error {
	// https://docs.gitlab.com/ee/api/releases/#get-the-latest-release
	resp, err := http.Get(latestRelease)
	if err != nil {
		return fmt.Errorf("failed to check for new version: %w", err)
	}

	content, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("failed to check for new version: %w", err)
	}

	release := struct {
		Tag   string `json:"tag_name"`
		Links struct {
			Self string `json:"self"`
		} `json:"_links"`
	}{}
	err = json.Unmarshal(content, &release)
	if err != nil {
		return fmt.Errorf("failed to check for new version: %w", err)
	}

	if release.Tag != Version {
		cmd.SilenceUsage = true
		return fmt.Errorf("new version available: %s %s", release.Tag, release.Links.Self)
	} else {
		fmt.Println("up-to-date")
	}

	return nil
}
