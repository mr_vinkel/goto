package gt

func GitHubShortcuts() []Shortcut {
	return []Shortcut{
		{
			Name:        "repository",
			Alias:       "r",
			Description: "Open repository in the browser",
			Host:        "github.com",
			Target:      "https://${git.remote.host}/${git.remote.path}",
			Parameters: []Parameter{
				{
					Name:           "branch",
					Alias:          "b",
					Description:    "Open for specified branch",
					Target:         "https://${git.remote.host}/${git.remote.path}/tree/${param.branch}",
					CompletionFunc: "branches",
				},
				{
					Name:        "current",
					Alias:       "c",
					Description: "Open for current branch",
					Flag:        true,
					Target:      "https://${git.remote.host}/${git.remote.path}/tree/${git.branch.current}",
				},
			},
		},
		{
			Name:        "repository graph",
			Alias:       "g",
			Description: "Open repository graph in the browser",
			Host:        "github.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/network",
		},
		{
			Name:        "repository branches",
			Alias:       "b",
			Description: "Open repository branches in the browser",
			Host:        "github.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/branches",
		},
		{
			Name:        "repository tags",
			Alias:       "t",
			Description: "Open repository branches in the browser",
			Host:        "github.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/tags",
		},
		{
			Name:        "repository releases",
			Alias:       "rl",
			Description: "Open repository releases in the browser",
			Host:        "github.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/releases",
			Parameters: []Parameter{
				{
					Name:        "latest",
					Alias:       "l",
					Description: "Open latest release",
					Flag:        true,
					Target:      "https://${git.remote.host}/${git.remote.path}/releases/latest",
				},
				{
					Name:           "tag",
					Alias:          "t",
					Description:    "Open release for specified tag",
					Target:         "https://${git.remote.host}/${git.remote.path}/releases/tag/${param.tag}",
					CompletionFunc: "tags",
				},
			},
		},
		{
			Name:        "pipeline",
			Alias:       "p",
			Description: "Open current repository pipelines",
			Host:        "github.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/actions",
			Parameters: []Parameter{
				{
					Name:           "branch",
					Alias:          "b",
					Description:    "Open actions for specified branch",
					Target:         "https://${git.remote.host}/${git.remote.path}/actions?query=branch%3A${param.branch}",
					CompletionFunc: "branches",
				},
				{
					Name:        "current",
					Alias:       "c",
					Description: "Open actions for current branch",
					Flag:        true,
					Target:      "https://${git.remote.host}/${git.remote.path}/actions?query=branch%3A${git.branch.current}",
				},
			},
		},
		{
			Name:        "merge requests",
			Alias:       "m",
			Description: "Open current repository merge requests",
			Host:        "github.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/pulls",
			Parameters: []Parameter{
				{
					Name:        "create",
					Alias:       "c",
					Description: "Create new merge request for current branch",
					Flag:        true,
					Target:      "https://${git.remote.host}/${git.remote.path}/compare/${git.branch.default}...${git.branch.current}?quick_pull=1",
				},
			},
		},
	}
}
