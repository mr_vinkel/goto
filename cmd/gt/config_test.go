package gt_test

import (
	"goto/cmd/gt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadConfigCreatesFile(t *testing.T) {
	file := "../../testdata/config/TestReadConfigCreatesFile.yaml.ignore"
	_ = os.Remove(file)

	os.Setenv(gt.EnvVarConfigFile, file)

	_, err := gt.ReadConfig()
	assert.Nil(t, err)

	_, err = os.Stat(file)
	assert.Nil(t, err)
}

func TestReadConfig(t *testing.T) {
	file := "../../testdata/config/TestReadConfig.yaml"
	os.Setenv(gt.EnvVarConfigFile, file)

	config, err := gt.ReadConfig()
	assert.Nil(t, err)

	assert.Equal(t, "1.0", config.Version)
	assert.Equal(t, 1, len(config.Globals))
	assert.Equal(t, "gohome", config.Globals[0].Name)
	assert.Equal(t, "Go home!", config.Globals[0].Description)
	assert.Equal(t, "h", config.Globals[0].Alias)
	assert.Equal(t, "$env.HOME", config.Globals[0].Target)

	assert.Equal(t, 1, len(config.Repositories))
	assert.Equal(t, "repo", config.Repositories[0].Name)
	assert.Equal(t, "Open repository", config.Repositories[0].Description)
	assert.Equal(t, "gitlab.com", config.Repositories[0].Host)
	assert.Equal(t, "r", config.Repositories[0].Alias)
	assert.Equal(t, "https://${git.remote.host}/${git.remote.path}", config.Repositories[0].Target)
	assert.Equal(t, 1, len(config.Repositories[0].Parameters))
	assert.Equal(t, "branch", config.Repositories[0].Parameters[0].Name)
	assert.Equal(t, "b", config.Repositories[0].Parameters[0].Alias)
	assert.Equal(t, "Open repository for branch", config.Repositories[0].Parameters[0].Description)
	assert.Equal(t, "https://${git.remote.host}/${git.remote.path}/-/tree/${param.branch}", config.Repositories[0].Parameters[0].Target)
	assert.Equal(t, false, config.Repositories[0].Parameters[0].Flag)
}
