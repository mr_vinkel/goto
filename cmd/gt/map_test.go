package gt_test

import (
	"goto/cmd/gt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type maptest struct {
	Path  string
	Value interface{}
}

func TestGet(t *testing.T) {
	data := map[string]interface{}{
		"git": map[string]interface{}{
			"remote": map[string]interface{}{
				"raw":        "git@gitlab.com:mr_vinkel/goto.git",
				"parsed":     "https://gitlab.com/mr_vinkel/goto",
				"host":       "gitlab.com",
				"hostname":   "gitlab.com",
				"path":       "mr_vinkel/goto",
				"repository": "goto",
				"group":      "mr_vinkel",
			},
			"branch": map[string]interface{}{
				"current": "main",
			},
		},
		"args": map[string]interface{}{
			"count":   2,
			"all":     []string{"potat", "tomat"},
			"0":       "potat",
			"1":       "tomat",
			"joined":  "potat tomat",
			"escaped": "potat+tomat",
		},
	}

	tests := []maptest{
		{Path: "", Value: nil},
		{Path: ".", Value: data},
		{Path: "git.branch.current", Value: "main"},
		{Path: ".git.branch.current", Value: "main"},
		{Path: "args.count", Value: 2},
		{Path: "args.all", Value: []string{"potat", "tomat"}},
		{Path: "lol", Value: nil},
		{Path: "rofl.lol", Value: nil},
	}

	for _, test := range tests {
		t.Run(test.Path, func(t *testing.T) {
			result := gt.Get(data, test.Path)
			assert.Equal(t, test.Value, result)
		})
	}
}

func TestSet(t *testing.T) {
	data := map[string]interface{}{
		"git": map[string]interface{}{
			"remote": map[string]interface{}{
				"raw":        "git@gitlab.com:mr_vinkel/goto.git",
				"parsed":     "https://gitlab.com/mr_vinkel/goto",
				"host":       "gitlab.com",
				"hostname":   "gitlab.com",
				"path":       "mr_vinkel/goto",
				"repository": "goto",
				"group":      "mr_vinkel",
			},
			"branch": map[string]interface{}{
				"current": "main",
			},
		},
		"args": map[string]interface{}{
			"count":   2,
			"all":     []string{"potat", "tomat"},
			"0":       "potat",
			"1":       "tomat",
			"joined":  "potat tomat",
			"escaped": "potat+tomat",
		},
	}

	tests := []maptest{
		{Path: "", Value: nil},
		{Path: ".", Value: data},
		{Path: "git.branch.current", Value: "potot"},
		{Path: ".git.branch.current", Value: "tomat"},
		{Path: "args.count", Value: 3},
		{Path: "args.all", Value: []string{"potat"}},
		{Path: "lol", Value: nil},
		{Path: "rofl", Value: "lol"},
		{Path: "rofl.lol", Value: nil},
	}

	for _, test := range tests {
		t.Run(test.Path, func(t *testing.T) {
			gt.Set(data, test.Path, test.Value)
			result := gt.Get(data, test.Path)
			assert.Equal(t, test.Value, result)
		})
	}
}
