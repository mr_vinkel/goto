package gt

func GitLabShortcuts() []Shortcut {
	return []Shortcut{
		{
			Name:        "repository",
			Alias:       "r",
			Description: "Open repository in the browser",
			Host:        "gitlab.com",
			Target:      "https://${git.remote.host}/${git.remote.path}",
			Parameters: []Parameter{
				{
					Name:           "branch",
					Alias:          "b",
					Description:    "Open for specified branch",
					Target:         "https://${git.remote.host}/${git.remote.path}/-/tree/${param.branch}",
					CompletionFunc: "branches",
				},
				{
					Name:        "current",
					Alias:       "c",
					Description: "Open for current branch",
					Flag:        true,
					Target:      "https://${git.remote.host}/${git.remote.path}/-/tree/${git.branch.current}",
				},
			},
		},
		{
			Name:        "repository graph",
			Alias:       "g",
			Description: "Open repository graph in the browser",
			Host:        "gitlab.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/-/network/main",
			Parameters: []Parameter{
				{
					Name:           "branch",
					Alias:          "b",
					Description:    "Open for specified branch",
					Target:         "https://${git.remote.host}/${git.remote.path}/-/network/${param.branch}",
					CompletionFunc: "branches",
				},
				{
					Name:        "current",
					Alias:       "c",
					Description: "Open for current branch",
					Flag:        true,
					Target:      "https://${git.remote.host}/${git.remote.path}/-/network/${git.branch.current}",
				},
			},
		},
		{
			Name:        "repository branches",
			Alias:       "b",
			Description: "Open repository branches in the browser",
			Host:        "gitlab.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/-/branches",
		},
		{
			Name:        "repository tags",
			Alias:       "t",
			Description: "Open repository branches in the browser",
			Host:        "gitlab.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/-/tags",
		},
		{
			Name:        "repository releases",
			Alias:       "rl",
			Description: "Open repository releases in the browser",
			Host:        "gitlab.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/-/releases",
			Parameters: []Parameter{
				{
					Name:        "latest",
					Alias:       "l",
					Description: "Open latest release",
					Flag:        true,
					Target:      "https://${git.remote.host}/${git.remote.path}/-/releases/permalink/latest",
				},
				{
					Name:           "tag",
					Alias:          "t",
					Description:    "Open release for specified tag",
					Target:         "https://${git.remote.host}/${git.remote.path}/-/releases/${param.tag}",
					CompletionFunc: "tags",
				},
			},
		},
		{
			Name:        "pipeline",
			Alias:       "p",
			Description: "Open current repository pipelines",
			Host:        "gitlab.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/-/pipelines",
			Parameters: []Parameter{
				{
					Name:           "branch",
					Alias:          "b",
					Description:    "Open latest for specified branch",
					Target:         "https://${git.remote.host}/${git.remote.path}/-/pipelines/${param.branch}/latest",
					CompletionFunc: "branches",
				},
				{
					Name:           "tag",
					Alias:          "t",
					Description:    "Open latest for specified tag",
					Target:         "https://${git.remote.host}/${git.remote.path}/-/pipelines/${param.tag}/latest",
					CompletionFunc: "tags",
				},
				{
					Name:        "current",
					Alias:       "c",
					Description: "Open latest for current branch",
					Flag:        true,
					Target:      "https://${git.remote.host}/${git.remote.path}/-/pipelines/${git.branch.current}/latest",
				},
			},
		},
		{
			Name:        "merge requests",
			Alias:       "mr",
			Description: "Open current repository merge requests",
			Host:        "gitlab.com",
			Target:      "https://${git.remote.host}/${git.remote.path}/-/merge_requests",
			Parameters: []Parameter{
				{
					Name:        "create",
					Alias:       "c",
					Description: "Create new merge request for current branch",
					Flag:        true,
					Target:      "https://${git.remote.host}/${git.remote.path}/-/merge_requests/new?merge_request%5Bsource_branch%5D=${git.branch.current}",
				},
			},
		},
	}
}
