package gt

import (
	"bytes"
	"fmt"
	"net/url"
	"os"
	"strings"
	"text/template"

	"github.com/Masterminds/sprig"
	"github.com/spf13/cobra"
)

var (
	cmds = map[string]map[string]Shortcut{}
)

func AddShortcuts(rootCmd *cobra.Command, config *Config) error {
	if err := add(rootCmd, GitLabShortcuts(), true); err != nil {
		return err
	}
	if err := add(rootCmd, GitHubShortcuts(), true); err != nil {
		return err
	}
	if err := add(rootCmd, config.Globals, false); err != nil {
		return err
	}
	if err := add(rootCmd, config.Repositories, true); err != nil {
		return err
	}
	return nil
}

func add(rootCmd *cobra.Command, shortcuts []Shortcut, repository bool) error {
	for _, shortcut := range shortcuts {
		host := "default"
		if shortcut.Host != "" {
			host = shortcut.Host
		}

		if e, ok := cmds[shortcut.Alias]; ok {
			e[host] = shortcut
			continue
		}

		cmd := &cobra.Command{
			Use:   shortcut.Alias,
			Short: shortcut.Description,
			RunE: func(cmd *cobra.Command, args []string) error {
				return execute(cmd, args, repository)
			},
			ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
				return []string{}, cobra.ShellCompDirectiveNoFileComp
			},
		}

		for _, parameter := range shortcut.Parameters {
			if len(parameter.Alias) != 1 {
				return fmt.Errorf("parameter alias must be one char! shortcut: %s, parameter: %s\n", shortcut.Name, parameter.Name)
			}
			if parameter.Flag {
				cmd.Flags().BoolP(parameter.Name, parameter.Alias, false, parameter.Description)
			} else {
				cmd.Flags().StringP(parameter.Name, parameter.Alias, "", parameter.Description)
			}

			// Setup parameter completion
			completionFunc := strings.ToLower(parameter.CompletionFunc)
			if completionFunc == "" {
				completionFunc = "none"
			}
			switch completionFunc {
			case "branches":
				err := cmd.RegisterFlagCompletionFunc(parameter.Name, func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
					branches, err := GetBranches(".")
					if err != nil {
						fmt.Printf("Error: %s\n", err)
					}
					return branches, cobra.ShellCompDirectiveNoFileComp
				})
				if err != nil {
					return err
				}

			case "tags":
				err := cmd.RegisterFlagCompletionFunc(parameter.Name, func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
					branches, err := GetTags(".")
					if err != nil {
						fmt.Printf("Error: %s\n", err)
						return nil, cobra.ShellCompDirectiveNoFileComp
					}
					return branches, cobra.ShellCompDirectiveNoFileComp
				})
				if err != nil {
					return err
				}

			case "files":
				// do nothing = default file and directory completion
				break

			case "none":
				if err := cmd.RegisterFlagCompletionFunc(parameter.Name, cobra.NoFileCompletions); err != nil {
					return err
				}

			default:
				return fmt.Errorf("invalid completionFunc: %s for shortcut: %s and parameter: %s\n", completionFunc, shortcut.Name, parameter.Name)
			}
		}
		cmds[shortcut.Alias] = make(map[string]Shortcut)
		cmds[shortcut.Alias][host] = shortcut
		rootCmd.AddCommand(cmd)
	}
	return nil
}

func execute(cmd *cobra.Command, args []string, repository bool) error {
	debug, err := cmd.Flags().GetBool("debug")
	if err != nil {
		return err
	}

	c, ok := cmds[cmd.Name()]
	if !ok {
		return fmt.Errorf("unknown command: %s", cmd.Name())
	}

	// Convert args to map
	data := map[string]interface{}{}
	Set(data, "arg.count", len(args))
	Set(data, "arg.all", args)
	Set(data, "arg.joined", strings.Join(args, " "))
	Set(data, "arg.escaped", url.QueryEscape(strings.Join(args, " ")))
	for i, arg := range args {
		Set(data, fmt.Sprintf("arg.%d", i), arg)
	}

	// Lookup git repository
	host := "default"
	if repository {
		git, err := GetRepository(".")
		if err != nil {
			return err
		}
		Set(data, "git", git)
		host = GetS(data, "git.remote.hostname")
		if host == "" {
			host = "default"
		}
	}

	s, ok := c[host]
	if !ok {
		return fmt.Errorf("no shortcut for host: %s", host)
	}

	// Look up parameters and target
	target := s.Target
	templating := s.Templating
	for _, param := range s.Parameters {
		if param.Flag {
			value, err := cmd.Flags().GetBool(param.Name)
			if err == nil && value {
				if param.Target != "" {
					target = param.Target
				}
				if param.Templating {
					templating = true
				}
			}
		} else {
			value, err := cmd.Flags().GetString(param.Name)
			if err == nil && value != "" {
				Set(data, fmt.Sprintf("param.%s", param.Name), value)
				Set(data, fmt.Sprintf("param.%s", param.Alias), value)
				if param.Target != "" {
					target = param.Target
				}
				if param.Templating {
					templating = true
				}
			}
		}
	}

	var expandedTarget string
	if templating {
		expandedTarget, err = templateTarget(data, target)
		if err != nil {
			return err
		}
	} else {
		expandedTarget = os.Expand(target, func(s string) string { return GetS(data, s) })
	}

	// Expand and exec target
	if debug {
		Print(data)
		fmt.Printf("templating: %t\n", templating)
		fmt.Printf("target: '%s' -> '%s'\n", target, expandedTarget)
	}
	link, err := url.Parse(expandedTarget)
	if err != nil {
		return err
	}
	return OpenBrowser(link)
}

func templateTarget(data map[string]interface{}, target string) (string, error) {
	template, err := template.New("base").Funcs(sprig.FuncMap()).Parse(target)
	if err != nil {
		return "", err
	}
	result := bytes.NewBuffer(make([]byte, 0))
	err = template.Execute(result, data)
	return result.String(), err
}
