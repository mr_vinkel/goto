package gt

import (
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
)

type repository struct {
	name        string
	absolute    string
	basedname   string
	basedParent string
}

func RepositoryCmd(config *Config) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "repo",
		Short: "Seach and goto repository",
		Args:  cobra.MaximumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			repoName := ""
			if len(args) != 0 {
				repoName = args[0]
			}

			repos, err := searchRepository(repoName, config)
			if err != nil {
				return err
			}
			return printResults(cmd, &repos, repoName)
		},
		ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			repos, err := searchRepository(toComplete, config)
			repoList := []string{}
			if err != nil {
				fmt.Printf("Error: %s", err)
			}
			for k := range repos {
				repoList = append(repoList, k)
			}
			return repoList, cobra.ShellCompDirectiveNoFileComp
		},
	}
	cmd.Flags().BoolP("verbose", "v", false, "Print full paths of repositories")
	return cmd
}

func searchRepository(repoName string, config *Config) (map[string]repository, error) {
	dirs := config.SearchDirs
	if len(dirs) == 0 {
		dirs = append(dirs, "$HOME")
	}

	conflict := &map[string]repository{}
	result := &map[string]repository{}

	for _, dir := range dirs {
		dir := os.ExpandEnv(dir)
		if !strings.HasPrefix(dir, "/") {
			fmt.Printf("%s is not an absolute path\n", dir)
			continue
		}

		err := seachDir(dir, conflict, result)
		if err != nil {
			fmt.Println(err)
		}
	}

	result = filterResults(result, repoName)
	return *result, nil
}

func seachDir(dir string, conflict *map[string]repository, result *map[string]repository) error {
	files, err := os.ReadDir(dir)
	if err != nil {
		return err
	}

	// search directories recursive for '.git' folder
	for _, file := range files {
		if file.IsDir() && file.Name() == ".git" {
			a := dir
			b := path.Base(dir)
			p := path.Dir(dir)
			(*result)[b] = repository{name: b, absolute: a, basedname: b, basedParent: p}
			break
		} else if file.IsDir() {
			err := seachDir(path.Join(dir, file.Name()), conflict, result)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func filterResults(repos *map[string]repository, repoName string) *map[string]repository {
	if repoName == "" {
		return repos
	}

	result := make(map[string]repository)
	for k, r := range *repos {
		if strings.HasPrefix(r.basedname, repoName) {
			result[r.basedname] = (*repos)[k]
		}
	}
	return &result
}

func printResults(cmd *cobra.Command, result *map[string]repository, repoName string) error {
	verbose, err := cmd.Flags().GetBool("verbose")
	if err != nil {
		return err
	}

	if len(*result) == 0 {
		if repoName != "" {
			cmd.SilenceUsage = true
			return fmt.Errorf("no repos matches '%s'", repoName)
		}
		return fmt.Errorf("no repositories found")
	}

	out := ""
	first := true
	for _, r := range *result {
		if first {
			first = false
		} else {
			out += " "
		}
		if verbose {
			if repoName == r.basedname {
				out = r.absolute
				break
			}
			out = out + r.absolute
		} else {
			out = out + r.basedname
		}
	}

	fmt.Printf("%s\n", out)
	return nil
}
