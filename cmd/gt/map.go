package gt

import (
	"fmt"
	"reflect"
	"strings"

	"gopkg.in/yaml.v3"
)

func GetS(m map[string]interface{}, path string) string {
	return fmt.Sprintf("%v", Get(m, path))
}

func Get(m map[string]interface{}, path string) interface{} {
	if path == "." {
		return m
	}
	path = strings.TrimLeft(path, ".")
	elements := strings.Split(path, ".")
	lenElements := len(elements)

	current := m
	for i := 0; i < lenElements; i++ {
		v, ok := current[elements[i]]
		// path did not map to a value
		if !ok {
			return nil
		}
		// we found it yay
		if i+1 == lenElements {
			return v
		}
		// we must go deeper
		t := reflect.ValueOf(v)
		if t.Kind() == reflect.Map {
			current = v.(map[string]interface{})
		} else {
			// path ends here
			return nil
		}
	}
	return nil
}

func Set(m map[string]interface{}, path string, value interface{}) {
	path = strings.TrimLeft(path, ".")
	elements := strings.Split(path, ".")
	lenElements := len(elements)

	current := m
	for i := 0; i < lenElements; i++ {
		if i+1 == lenElements {
			// yay we found it
			current[elements[i]] = value
			return
		}

		v, ok := current[elements[i]]
		if ok {
			t := reflect.ValueOf(v)
			if t.Kind() == reflect.Map {
				// use existing map
				current = v.(map[string]interface{})
				continue
			}
		}
		// we must go deeper
		m := make(map[string]interface{})
		current[elements[i]] = m
		current = m
	}
}

func Print(m map[string]interface{}) {
	out, err := yaml.Marshal(m)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
	fmt.Println(string(out))
}
