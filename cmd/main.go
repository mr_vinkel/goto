package main

import (
	"fmt"
	"goto/cmd/gt"
	"os"

	"github.com/spf13/cobra"
)

func main() {
	config, err := gt.ReadConfig()
	if err != nil {
		fmt.Printf("failed to read config: %v\n", err)
		os.Exit(1)
	}

	rootCmd := &cobra.Command{
		Use:   "gt",
		Short: "Goto - open and goto things easily",
	}
	rootCmd.PersistentFlags().BoolP("debug", "d", false, "Enable debug")

	rootCmd.AddCommand(gt.VersionCmd())
	rootCmd.AddCommand(gt.ConfigCmd(config))
	rootCmd.AddCommand(gt.RepositoryCmd(config))

	if err := gt.AddShortcuts(rootCmd, config); err != nil {
		fmt.Printf("Error: %v", err)
		os.Exit(1)
	}

	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
