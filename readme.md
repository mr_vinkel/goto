# GoTo

Small app for opening links from cli.

This is not a CLI for GitLab or GitHub - this is just a small tool for opening links for the current repository fast.

## Usage

### Global parameters

`-h` or `--help` can be used to show help

`-d` or `--debug` can be used to debug any shortcut command

### Version

`gt version` shows the current version of gt. Use `gt version -c` to check for new version.

### Bash completion

`gt completion` generates bash completion. This should be reloaded every time the config changes. Example put `source <(gt completion bash)` in bash `~/.profile` and restart/reload shell on config changes.

### Repository navigation

`gt repo` searches `$HOME` by default for git repositories. The search repository can be configured to search in multiple directories in the config file. Repository search can be used together with completion to seach and change directory to a repository. It is not allowed for a process to change the directory of the parent, therefore the gt command must be wrapped in a function i.e. a bash function.

In bash:

```bash
source <(gt completion bash)
unset -f gt
export GT_HOME=$(which gt)

# map gt to a bash function to allow cd'ing
gt() {
    if [ "$#" == "2" ] && [ "$1" == "repo" ]; then
        cd $($GT_HOME repo -v $2)
        return
    fi
    $GT_HOME "$@"
}
```

### Repository commands

gt collects information from git about the repository. All repository commands uses the git remote to determine the url for the repository. Different repository providers have different URL schemes. See [Custom shortcuts](#custom-shortcuts) on how to configure a new repository provider and add new shortcuts.

| Command            | GitLab | GitHub | Description                                  |
| ------------------ | ------ | ------ | -------------------------------------------- |
| `gt r`             | [x]    | [x]    | Open repository code in browser              |
| `gt r -c`          | [x]    | [x]    | Open repository code for the current branch  |
| `gt r -b <branch>` | [x]    | [x]    | Open repository code for a branch            |
| `gt g`             | [x]    | [x]    | Open repository graph/network                |
| `gt g -c`          | [x]    | [ ]    | Open repository graph for the current branch |
| `gt g -b <branch>` | [x]    | [ ]    | Open repository graph for a branch           |
| `gt b`             | [x]    | [x]    | Open repository branches                     |
| `gt t`             | [x]    | [x]    | Open repository tags                         |
| `gt rl`            | [x]    | [x]    | Open repository releases                     |
| `gt rl -l`         | [x]    | [x]    | Open latest repository release               |
| `gt rl -t <tag>`   | [x]    | [x]    | Open repository release for a tag            |
| `gt p`             | [x]    | [x]    | Open pipelines/actions                       |
| `gt p -c`          | [x]    | [x]    | Open pipeline/actions for the current branch |
| `gt p -b <branch>` | [x]    | [x]    | Open pipeline/actions for a branch           |
| `gt p -t <tag>`    | [x]    | [ ]    | Open pipeline for a tag                      |
| `gt mr`            | [x]    | [x]    | Open repository merge requests               |
| `gt mr -c`         | [x]    | [x]    | Create merge request for the current branch  |

### Custom shortcuts

gt creates a config file in `${HOME}/.config/gt/config.yaml` - this can be changed by setting the `GT_CONFIG_FILE` environment variable to another path. The config file can be opened with `gt config`

Shortcuts are configured with a target URL that gt sohuld open. The target url can be expanded with values from git, parameters and environment variables. The following stardard variables can be used for expansion. Note Git variables are only set when in a git repository.

| Variable                | Description                                                                         | Example                                                                    |
| ----------------------- | ----------------------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| `arg.count`             | Number of positionel arguments                                                      | `gt google hello world` will set `arg.count` to `2`                        |
| `arg.<number>`          | Positionel argument value                                                           | `gt google hello world` will set `arg.0` to `hello` and `arg.1` to `world` |
| `arg.all`               | String slice of all positionel arguments                                            | `gt google hello world` will set `arg.all` to `[hello, world]`             |
| `arg.joined`            | Joined string of all positionel arguments                                           | `gt google hello world` will set `arg.joined` to `hello world`             |
| `arg.escaped`           | Joined string of all positionel arguments url query escaped                         | `gt google hello world` will set `arg.escaped` to `hello+world`            |
| `param.<name>`          | Parameter value                                                                     | `gt r --branch test` will set `param.branch` to `test`                     |
| `git.branch.default`    | Best guess on the repos default branch                                              | `main` or `master`                                                         |
| `git.branch.current`    | Current branch of the repository                                                    | `feature/brew-coffee`                                                      |
| `git.remote.raw`        | The origin remote of the repository                                                 | `git@gitlab.com:mr_vinkel/goto.git`                                        |
| `git.remote.parsed`     | Remote parsed into an URL                                                           | `https://gitlab.com/mr_vinkel/goto`                                        |
| `git.remote.sheme`      | Scheme part of the remote                                                           | `ssh`                                                                      |
| `git.remote.host`       | Host part of the remote with port number (if available)                             | `gitlab.com`                                                               |
| `git.remote.hostname`   | Host part of the remote without port number                                         | `gitlab.com`                                                               |
| `git.remote.path`       | Path part of the remote                                                             | `mr_vinkel/goto`                                                           |
| `git.remote.repository` | Last element in the remote path (the repository name)                               | `goto`                                                                     |
| `git.remote.group`      | Remote path excluding the repository - this is typically the group, user or project | `mr_vinkel`                                                                |

The config file can be used to configure custom shortcuts:

```yaml
version: "1.0"
# Editor to open this file in with 'gt config' - default is nano
editor: nano
# Directories to search for git repositories. If the search path contains many files this will be slow - default is $HOME
searchDirs:
- $HOME
globals:
# Global shortcuts can be called from anywhere
- name: google
  alias: google
  description: Open google
  target: https://google.com/search?q=${arg.escaped}

# Repository shortcuts only work when in a git repository
repositories:
- name: repository
  alias: r
  description: Open repository
  # Host is used to only use this target for remotes with this host - allowing for using the same shortcut alias for github, gitlab etc.
  host: my-server.com
  target: https://my-server.com/${git.remote.repository}
  parameters:
  - name: branch
    alias: b
    description: Open branch
    # Target can be overridden with parameters
    target: https://my-server.com/${git.remote.repository}?ref=${param.branch}
    # CompletionFunc can be used specify completion values. Default is none. Allowed functions are: none, files, branches, tags
    completionFunc: branches
  - name: current
    alias: c
    description: Current branch
    # Flags can be used to specify a parameter without input
    flag: true
    # Use golang text templating instead of expand - see https://pkg.go.dev/text/template and https://masterminds.github.io/sprig
    templating: true
    target: https://my-server.com/{{.git.remote.repository}}?ref={{.git.branch.current|lower}}
```
